Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: minimap2
Upstream-Contact: Heng Li <hengli@broadinstitute.org>
Source: https://github.com/lh3/minimap2
Files-Excluded: */natbib.*, sse2neon/*

Files: *
Copyright: © 2018-     Dana-Farber Cancer Institute
             2017-2018 Broad Institute <hengli@broadinstitute.org>
License: MIT

Files: tex/bioinfo.cls
Copyright: © 2018-     Dana-Farber Cancer Institute
           © 2017-2018 Broad Institute <hengli@broadinstitute.org>
           © 2000 Part of code borrowed from  Peter Wilson and Donald Arseneau
License: MIT

Files: misc.c
Copyright: 2003 SRA, Inc., SKC, Inc.
Comment: gettimeofday.c
    Win32 gettimeofday() replacement
    taken from PostgreSQL, according to
    https://stackoverflow.com/questions/1676036/what-should-i-use-to-replace-gettimeofday-on-windows
 .
   src/port/gettimeofday.c
License: PostgreSQL
 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose, without fee, and without a
 written agreement is hereby granted, provided that the above
 copyright notice and this paragraph and the following two
 paragraphs appear in all copies.
 .
 IN NO EVENT SHALL THE AUTHOR BE LIABLE TO ANY PARTY FOR DIRECT,
 INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING
 LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 DOCUMENTATION, EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 THE AUTHOR SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS
 IS" BASIS, AND THE AUTHOR HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE,
 SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

Files: kseq.h
Copyright: 2008, 2009, 2011 Attractive Chaos <attractor@live.co.uk>
License: MIT

Files: debian/*
Copyright: © 2018 Andreas Tille <tille@debian.org>
             2020 Steffen Moeller <moeller@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
